To start contributing, your first place to look should ideally be the [issues page](https://github.com/trvlrr/ghost/issues).
If there's nothing there for you to do, work on new features or improve the code!

Always make sure you create a fork off of ghost's **dev** branch if you plan to contribute. Keep your fork updated to
avoid merge conflicts (see [this](https://robots.thoughtbot.com/keeping-a-github-fork-updated) guide).

General guidelines:
* Do **NOT** update libraries or their dependencies.
* Follow the [style guide](https://github.com/trvlrr/ghost/wiki/Style-Guide).
* Fewer commits are better in most cases.
* Write proper commit messages that explain the changes you made.

Adding new methods or classes is fine, assuming:
1. Non-internal methods, classes, and packages are documented with [JavaDoc](http://www.oracle.com/technetwork/java/javase/documentation/index-137868.html).
2. Existing behavior is kept the same, or if absolutely necessary, similar.
3. Major changes to code are explained to a reasonable extent.
